<?php
//GET GATES INFORMATIONS--------------------
$url = "https://api.flightstats.com/flex/fids/rest/v1/json/DEN/";
$ch = curl_init();
$headers = array(
    'Accept: application/json',
    'Content-Type: application/json',
);
$body = array(
	'appId'=>'741e686e',
	'appKey'=>'78e1a49bb366b93c20d064e3eb875374',
	'requestedFields'=>'airlineName,flightNumber,flightId,city,currentDate,currentTime,gate,remarks',
	'timeFormat'=>'24',
	'sortFields'=>'currentDate,currentTime'
);
curl_setopt($ch, CURLOPT_URL, $url.'departures?'.http_build_query($body));
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_URL, $url.'departures?'.http_build_query($body));
$departures = json_decode(curl_exec($ch));

curl_setopt($ch, CURLOPT_URL, $url.'arrivals?'.http_build_query($body));
$arrivals = json_decode(curl_exec($ch));

//------------------------------------------
//------------------------------------------
$data = [
//		['averageDwellTime'=>0,"beacon"=>"Denver_14","gate"=>" ","airline"=>"Airlines","date"=>"","count"=>0,"lat"=>"39.85294245492" ,"lng"=>"-104.67360154575499"],
/*
	['averageDwellTime'=>0,"beacon"=>"Denver_02","gate"=>" ","airline"=>"United","date"=>"","count"=>0,"lat"=>"39.848186776434716","lng"=>"-104.6748098809162"  ],
	['averageDwellTime'=>0,"beacon"=>"Denver_03","gate"=>" ","airline"=>"","date"=>"","count"=>0,"lat"=>"39.85047868600109" ,"lng"=>"-104.67294038123282" ],
	['averageDwellTime'=>0,"beacon"=>"Denver_04","gate"=>" ","airline"=>"","date"=>"","count"=>0,"lat"=>"39.84994226658665" ,"lng"=>"-104.67390597647818" ],
	['averageDwellTime'=>0,"beacon"=>"Denver_05","gate"=>" ","airline"=>"","date"=>"","count"=>0,"lat"=>"39.84886323748695" ,"lng"=>"-104.67389792985114" ],
	['averageDwellTime'=>0,"beacon"=>"Denver_06","gate"=>"C38","airline"=>"","date"=>"","count"=>0,"lat"=>"39.86269473489172" ,"lng"=>"-104.6744665581623"  ],
	['averageDwellTime'=>0,"beacon"=>"Denver_07","gate"=>"C42","airline"=>"","date"=>"","count"=>0,"lat"=>"39.862797675667906","lng"=>"-104.6726855713764"  ],

	['averageDwellTime'=>0,"beacon"=>"Denver_08","gate"=>"B38B","airline"=>"United Airlines"  ,"date"=>"","count"=>0,"lat"=>"39.85852653314273" ,"lng"=>"-104.67303828186186"],
	['averageDwellTime'=>0,"beacon"=>"Denver_09","gate"=>"B35" ,"airline"=>"United Airlines"  ,"date"=>"","count"=>0,"lat"=>"39.85887357390808" ,"lng"=>"-104.67475489563151"],
	['averageDwellTime'=>0,"beacon"=>"Denver_10","gate"=>"A34" ,"airline"=>"Common Use Gate"  ,"date"=>"","count"=>0,"lat"=>"39.85358077594512" ,"lng"=>"-104.67572049087676"],
	['averageDwellTime'=>0,"beacon"=>"Denver_11","gate"=>"A42" ,"airline"=>"Frontier Airlines","date"=>"","count"=>0,"lat"=>"39.85359313048709" ,"lng"=>"-104.67265740818175"],
	['averageDwellTime'=>0,"beacon"=>"Denver_12","gate"=>"A45" ,"airline"=>"Common Use Gate"  ,"date"=>"","count"=>0,"lat"=>"39.85422732731909" ,"lng"=>"-104.67200831360014"],

	['averageDwellTime'=>0,"beacon"=>"Denver_01","gate"=>"601","airline"=>"Southwest","date"=>"","count"=>0,"lat"=>"39.84933891843599","lng"=>"-104.6731241125504"],
	['averageDwellTime'=>0,"beacon"=>"Denver_13","gate"=>"B57","airline"=>"Airlines","date"=>"","count"=>0,"lat"=>"39.85898878822237" ,"lng"=>"-104.6686089489499"],
	['averageDwellTime'=>0,"beacon"=>"Denver_15","gate"=>" ","airline"=>"Airlines","date"=>"","count"=>0,"lat"=>"39.850369659092095" ,"lng"=>"-104.67481926864787" ],
*/
	['averageDwellTime'=>0,"beacon"=>"Denver_02","gate"=>" "   ,"airline"=>"","date"=>"","count"=>0,"lat"=>"39.848186776434716","lng"=>"-104.6748098809162"  ],
	['averageDwellTime'=>0,"beacon"=>"Denver_03","gate"=>" "   ,"airline"=>"","date"=>"","count"=>0,"lat"=>"39.85047868600109" ,"lng"=>"-104.67294038123282" ],
	['averageDwellTime'=>0,"beacon"=>"Denver_04","gate"=>" "   ,"airline"=>"","date"=>"","count"=>0,"lat"=>"39.84994226658665" ,"lng"=>"-104.67390597647818" ],
	['averageDwellTime'=>0,"beacon"=>"Denver_05","gate"=>" "   ,"airline"=>"","date"=>"","count"=>0,"lat"=>"39.84886323748695" ,"lng"=>"-104.67389792985114" ],
	['averageDwellTime'=>0,"beacon"=>"Denver_15","gate"=>" "   ,"airline"=>"","date"=>"","count"=>0,"lat"=>"39.850369659092095","lng"=>"-104.67481926864787"],

	['averageDwellTime'=>0,"beacon"=>"Denver_06","gate"=>"C38" ,"airline"=>"","date"=>"","count"=>0,"lat"=>"39.86291760147734" ,"lng"=>"-104.67455842382105"  ],
	['averageDwellTime'=>0,"beacon"=>"Denver_07","gate"=>"C42" ,"airline"=>"","date"=>"","count"=>0,"lat"=>"39.86291605737008","lng"=>"-104.67276469654234"  ],
	['averageDwellTime'=>0,"beacon"=>"Denver_08","gate"=>"B38B","airline"=>"","date"=>"","count"=>0,"lat"=>"39.85855175517931" ,"lng"=>"-104.67304699904116" ],
	['averageDwellTime'=>0,"beacon"=>"Denver_09","gate"=>"B35" ,"airline"=>"","date"=>"","count"=>0,"lat"=>"39.858877581878076" ,"lng"=>"-104.67474819010886" ],
	['averageDwellTime'=>0,"beacon"=>"Denver_10","gate"=>"A34" ,"airline"=>"","date"=>"","count"=>0,"lat"=>"39.853691966742524" ,"lng"=>"-104.67583850807341" ],
	['averageDwellTime'=>0,"beacon"=>"Denver_11","gate"=>"A42" ,"airline"=>"","date"=>"","count"=>0,"lat"=>"39.853679612218386" ,"lng"=>"-104.6727915186325" ],
	['averageDwellTime'=>0,"beacon"=>"Denver_12","gate"=>"A45" ,"airline"=>"","date"=>"","count"=>0,"lat"=>"39.85395964755281" ,"lng"=>"-104.67190638965758" ],
	['averageDwellTime'=>0,"beacon"=>"Denver_01","gate"=>"601" ,"airline"=>"","date"=>"","count"=>0,"lat"=>"39.84933891843599" ,"lng"=>"-104.6731241125504"  ],
	['averageDwellTime'=>0,"beacon"=>"Denver_13","gate"=>"B57" ,"airline"=>"","date"=>"","count"=>0,"lat"=>"39.858848242096606" ,"lng"=>"-104.66859050876292"  ],
];

foreach($departures->fidsData as $flight){
	foreach($data as $key=>$d){
		if($d['gate'] == $flight->gate){
			$data[$key]['departure'] = $flight;
			break;
		}
	}
}

foreach($arrivals->fidsData as $flight){
	foreach($data as $key=>$d){
		if($d['gate'] == $flight->gate){
			$data[$key]['arrival'] = $flight;
			break;
		}
	}
}

$retVal =[
	"result" =>0,
	"total"  =>0,
	"data"   =>$data
];
$retVal['total'] = count($retVal['data']);
//----------------------------------------------------------------------
$mysql = mysqli_connect( 'localhost', 'root', 'EoMjulQ5yY', 'sniffer' );
mysqli_query($mysql, "SET NAMES 'utf8'");
if(mysqli_connect_errno()!=0){ echo json_encode($retVal); }
else{
	//------------------------------------------------------------------
	$result = mysqli_query($mysql, "SELECT `eventday` FROM `daily_sum_aggregates` order by `eventday` desc limit 1");
	if( $result!=false ){
		$eventday = mysqli_fetch_assoc($result);
		$eventday = $eventday['eventday'];
		$tmpDate  = date("Y-m-d", strtotime($eventday));
/*
		$qry =	"SELECT ".
					"daily_sum_aggregates.* , round(dwell_time.averageDwellTime/60) AS averageDwellTime ".
				"FROM  ".
					"`daily_sum_aggregates` ".
				"LEFT JOIN ( ".
					"SELECT ".
//						"beacon, SUM( `duration_sec` ) / COUNT( beacon ) AS averageDwellTime ".
						"beacon, AVG( duration_sec ) /60 AS averageDwellTime ".
					"FROM ".
						"apdata.dwell_time ".
					"WHERE ".
						"eventstarttime LIKE  '{$tmpDate}%' ".
					"GROUP BY ".
						"beacon ".
				") AS dwell_time ON dwell_time.beacon = daily_sum_aggregates.beacon ".
				"WHERE eventday LIKE  '{$tmpDate}%' ";
*/
		$qry =	"SELECT ".
					"daily_sum_aggregates.* , round(dwell_time.averageDwellTime) AS averageDwellTime ".
				"FROM  ".
					"`daily_sum_aggregates` ".
				"LEFT JOIN ( ".
					"SELECT ".
						"beacon, AVG(duration_sec)/60 AS averageDwellTime ".
					"FROM ".
						"sniffer.daily_dwell_time ".
					"GROUP BY ".
						"beacon ".
				") AS dwell_time ON dwell_time.beacon = daily_sum_aggregates.beacon ".
				"WHERE ".
					"eventday LIKE  '{$tmpDate}%' ";

		$result = mysqli_query($mysql, $qry);
//		$result = mysqli_query($mysql, "SELECT * FROM `daily_sum_aggregates` where `eventday`='{$eventday}'");
		if( $result!=false ){
			$db_apData = [];
			while($row=mysqli_fetch_assoc($result)){ $db_apData[]=$row; }
			for( $i=0; $i<count($retVal['data']); $i++){
				$beacon = strtolower(trim($retVal['data'][$i]['beacon']));
				$retVal['data'][$i]['date' ] = date("d/m/Y", strtotime($eventday));
				foreach($db_apData as $tmp){
					if($beacon==strtolower(trim($tmp['beacon']))){
						$retVal['data'][$i]['count'           ] = number_format($tmp['sum'], 0, '', ',');
						$retVal['data'][$i]['averageDwellTime'] = $tmp['averageDwellTime'];
					}
				}
			}
		}
	}
	//------------------------------------------------------------------
	for( $i=0; $i<count($retVal['data']); $i++){
		//--------------------------------------------------------------
		$retVal['data'][$i]['airline'] = '';
		//--------------------------------------------------------------
		$result = mysqli_query($mysql, "SELECT `airline` FROM apdata.airlines where `gate`='".$retVal['data'][$i]['gate']."' order by `gate`");
		if($result){
			//----------------------------------------------------------
			$row = mysqli_fetch_assoc($result);
			$retVal['data'][$i]['airline'] = $row['airline'];
			//----------------------------------------------------------
		}
	}
	//------------------------------------------------------------------
	$retVal['query'] = $qry;
	echo json_encode($retVal);
	//------------------------------------------------------------------
}
//----------------------------------------------------------------------

?>
