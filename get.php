<?php
$host='redshift-cluster1.cyekrcgfzfll.us-east-2.redshift.amazonaws.com';
$db = 'dev';
$username = 'redshift_master';
$password = '2h<$fP3E[<rFZh{u';

$dsn = "pgsql:host=$host;port=5439;dbname=$db;user=$username;password=$password";
try{
	$conn = new PDO($dsn);
	if($conn){
		$query = 
			"select ".
				"eventday ".
			"from ".
				"daily_aggregates ".
			"order by ".
				"eventday desc ".
			"limit 1;";
		$statement = $conn->prepare($query);
		$statement->execute();
		$row = $statement->fetch();
		if($row==null){ $eventday = '-'; }
		else{ $eventday = $row['eventday']; }
		$query = 
			"select ".
				"SUM(count) as sum, eventday,beacon  ".
			"from ".
				"daily_aggregates ".
			"where ".
				"eventday = '{$eventday}' ".
			"group by ".
				"beacon,eventday ".
			"order by ".
				"eventday;".
			"";
		$statement = $conn->prepare($query);
		$statement->execute();
		$results = $statement->fetchAll();
		$retVal = [];
		foreach($results as $tmp){ $retVal[] = ['beacon'=>$tmp['beacon'], 'sum'=>$tmp['sum'], 'eventday'=>$tmp['eventday']]; }
		echo json_encode( ['result'=>0, 'data'=>$retVal] );
	}else{ echo json_encode( ['result'=>1, 'msg'=>'Connection failed!'] ); }
}catch (PDOException $e){ echo json_encode( ['result'=>1, 'msg'=>$e->getMessage()] ); }
?>