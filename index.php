<?php
//------------------------------------------------------

//------------------------------------------------------
$f3=require('fatLib/base.php');
//------------------------------------------------------
$f3->set('DEBUG',1);
if ((float)PCRE_VERSION<7.9)
	trigger_error('PCRE version is out of date');
//------------------------------------------------------
// Load configuration
$f3->config('config.ini');
//------------------------------------------------------

//------------------------------------------------------
$f3->route('GET /',function($f3){
//	echo View::instance()->render('index.html');
	$f3->reroute('/sniffer');
});
//------------------------------------------------------
$f3->route('GET /sniffer',function($f3){
	echo View::instance()->render('sniffer.php');
});
//------------------------------------------------------
$f3->route('GET /login',function($f3){
	echo View::instance()->render('login.php');
});
$f3->route('GET /map',function($f3){
	echo View::instance()->render('map.php');
});
//------------------------------------------------------
//$f3->set('ONERROR',function($f3){ $f3->reroute('/login'); });
//------------------------------------------------------
/*
$f3->route('GET /records',function($f3){
	$f3->set('content','../ui/records.php');
	echo View::instance()->render('../ui/layout.htm');
});

$f3->route('GET /calibrate',function($f3){
	$f3->set('content','../ui/calibrate.php');
	echo View::instance()->render('../ui/calibrate.htm');
});
$f3->route('POST /ajax/calibrate',function($f3, $param){
//	$f3->set('id',$param['id']);
	echo View::instance()->render("../ajax/calibrate.php");
});
$f3->route('POST /ajax/beacons',function($f3, $param){
//	$f3->set('id',$param['id']);
	echo View::instance()->render("../ajax/beacons.php");
});
//------------------------------------------------------

//------------------------------------------------------
$request_body = file_get_contents('php://input');
$inData = json_decode($request_body, true);
//------------------------------------------------------
$f3->route('POST /parser/@type/@db/@device',function($f3, $param){
	global $inData;
	$f3->set('db',$param['db']);
	$f3->set('device',$param['device']);
	$f3->set('data',$inData);
	$type = trim(strtolower($param['type']));
	switch($type){
		case 'wifi':
		case 'ble':
			echo View::instance()->render("{$type}.php");
		break;
		default:
			$f3->reroute('/');
		break;
	}
});
//------------------------------------------------------
*/
//------------------------------------------------------
$f3->run();
//------------------------------------------------------
