<?php
if(isset($_SESSION['isLogin'])){ $_SESSION['isLogin']=false; }
?>
<html lang="en">
<head>
	<meta charset="<?=$ENCODING;?>" />
	<title>login</title>
	<meta http-equiv="refresh" content="300;" />
	<base href="<?= $SCHEME.'://'.$HOST.':'.$PORT.$BASE.'/'; ?>" />
	<link rel="stylesheet" href="ui/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="ui/css/bootstrap-table.css" type="text/css" />
	<link rel="stylesheet" href="ui/css/bootstrap-theme.min.css" type="text/css" />

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" 
		integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous" />
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

	<script type="text/javascript" src="ui/js/jquery.min.js"></script>
	<script type="text/javascript" src="ui/js/jquery-ui.js"></script>
	<script type="text/javascript" src="ui/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="ui/js/bootstrap-table.js"></script>

	<style>
@import url("http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,300,700");
@import url("http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600|Roboto Mono");

@font-face {
  font-family: 'Dosis';
  font-style: normal;
  font-weight: 300;
  src: local('Dosis Light'), local('Dosis-Light'), url(http://fonts.gstatic.com/l/font?kit=RoNoOKoxvxVq4Mi9I4xIReCW9eLPAnScftSvRB4WBxg&skey=a88ea9d907460694) format('woff2');
}
@font-face {
  font-family: 'Dosis';
  font-style: normal;
  font-weight: 500;
  src: local('Dosis Medium'), local('Dosis-Medium'), url(http://fonts.gstatic.com/l/font?kit=Z1ETVwepOmEGkbaFPefd_-CW9eLPAnScftSvRB4WBxg&skey=21884fc543bb1165) format('woff2');
}
body {
  background: #d2d6de;
    font-family: 'Source Sans Pro', 'Helvetica Neue', Arial, sans-serif,  Open Sans;
    font-size: 14px;
    line-height: 1.42857;
    height: 350px;
    padding: 0;
    margin: 0;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    font-weight: 400;
    overflow-x: hidden;
    overflow-y: auto;
    
}
.form-control {
    background-color: #ffffff;
    background-image: none;
    border: 1px solid #999999;
    border-radius: 0;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    color: #333333;
    display: block;
    font-size: 14px;
    height: 34px;
    line-height: 1.42857;
    padding: 6px 12px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    width: 100%;
}

.login-box, .register-box {
    width: 360px;
    margin: 7% auto;
}.login-page, .register-page {
    background: #d2d6de;
}

.login-logo, .register-logo {
    font-size: 35px;
    text-align: center;
    margin-bottom: 25px;
    font-weight: 300;
}.login-box-msg, .register-box-msg {
    margin: 0;
    text-align: center;
    padding: 0 20px 20px 20px;
}.login-box-body, .register-box-body {
    background: #fff;
    padding: 20px;
    border-top: 0;
    color: #666;
}.has-feedback {
    position: relative;
}
.form-group {
    margin-bottom: 15px;
}.has-feedback .form-control {
    padding-right: 42.5px;
}.login-box-body .form-control-feedback, .register-box-body .form-control-feedback {
    color: #777;
}
.form-control-feedback {
    position: absolute;
    top: 0;
    right: 0;
    z-index: 2;
    display: block;
    width: 34px;
    height: 34px;
    line-height: 34px;
    text-align: center;
    pointer-events: none;
}.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 10px;
    margin-bottom: 10px;
}.icheck>label {
    padding-left: 0;
}
.checkbox label, .radio label {
    min-height: 20px;
    padding-left: 20px;
    margin-bottom: 0;
    font-weight: 400;
    cursor: pointer;
}	</style>
</head>
<body style="margin:30px 60px;text-align:center;">
	<div class="login-box">
		<div class="login-logo"></div>
		<div class="login-box-body">
			<p class="login-box-msg">Sign in</p>
			<form action="http://myadcubes.com/index.php/user/auth/login" method="post" accept-charset="utf-8">
			</form>
			<div class="form-group has-feedback">
				<input type="text" id="username" value="" placeholder="Username" class="form-control" maxlength="10" size="30">
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
				<span><font color="red"></font></span>
			</div>
			<div class="form-group has-feedback">
				<input type="password" id="userpass" value="" placeholder="Password" class="form-control" maxlength="10" size="30">
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				<span><font color="red"></font></span>
			</div>
			<div class="row">
				<div class="col-xs-4">
					<input type="submit" value="Sign In" id="submit" class="btn btn-primary btn-block btn-flat" onClick="submit()" />
				</div><!-- /.col -->
			</div>
		</div><!-- /.login-box-body -->
	</div>
</body>
<script type="text/javascript">
//----------------------------------------

//----------------------------------------
$(function(){
	//------------------------------------
	$('#username').focus();
	//------------------------------------
	$('#username').on('keypress', function(e){ if(e.keyCode==13){ $('#userpass').focus(); } });
	$('#userpass').on('keypress', function(e){ if(e.keyCode==13){ $('#submit'  ).focus(); submit(); } });
	//------------------------------------
});
//----------------------------------------

//----------------------------------------
function submit(){
	var data = {};
	data.username = $("#username").val().trim();
	data.userpass = $("#userpass").val().trim();
	$.ajax({
		type: 'POST',
		url: '/ajax/login.php',
		dataType: 'json',
		data: data,
		error: function(xhr, textStatus, errorThrown){ console.log(xhr.status+" - "+errorThrown); },
		success: function(retVal){
			if(retVal.result==0){ window.location = "/map"; }
			else{ alert(retVal.msg); }
		}
	});
}
//----------------------------------------

//----------------------------------------
</script>
</html>