<?php
global $f3;
if(isset($_SESSION['isLogin'])){
	if($_SESSION['isLogin']!=true){ $f3->reroute('login'); }
}else{ $f3->reroute('login'); }
?>
<html lang="en">
<head>
	<meta charset="<?=$ENCODING;?>" />
	<title>map</title>
	<base href="<?= $SCHEME.'://'.$HOST.':'.$PORT.$BASE.'/'; ?>" />
	<link rel="stylesheet" href="ui/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="ui/css/bootstrap-table.css" type="text/css" />
	<link rel="stylesheet" href="ui/css/bootstrap-theme.min.css" type="text/css" />

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
		integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous" />

	<script type="text/javascript" src="ui/js/jquery.min.js"></script>
	<script type="text/javascript" src="ui/js/jquery-ui.js"></script>
	<script type="text/javascript" src="ui/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="ui/js/bootstrap-table.js"></script>

	<style>
		table th{ text-align:center !important; }
	</style>
</head>
<body style="margin:30px 60px;">
	<button class="btn map-btn btn-link" onClick="changeCenter(this, 39.85381551186161, -104.67387513107451)"><i class="fa fa-map-o"></i>&nbsp;Concourse A</button>
	<button class="btn map-btn btn-info" onClick="changeCenter(this, 39.85869124831563, -104.6735854525009)"><i class="fa fa-map-o"></i>&nbsp;Concourse B</button>
	<button class="btn map-btn btn-info" onClick="changeCenter(this, 39.86306840917178, -104.67367128318938)"><i class="fa fa-map-o"></i>&nbsp;Concourse C</button>
	<button class="btn map-btn btn-info" onClick="changeCenter(this, 39.84975487916211, -104.67397169059905)"><i class="fa fa-map-o"></i>&nbsp;Main Terminal</button>
	<button class="btn map-btn btn-info" onClick="window.location.href='http://178.128.229.93/denversniffer.html'"><i class="fa fa-map-o"></i>&nbsp;Preview Data</button>
	<!--<input type="button" onclick="location.href='http://google.com';" value="View Monthly Scanner Reports" />-->

	<button class="btn btn-danger" style="float:right" onClick="window.location='/login'"><i class="fa fa-sign-out"></i>&nbsp;logout</button>

	<div id="map" style="width:100%;height:90vh"></div>
</body>
<script type="text/javascript">
//------------------------------------------
var mainMap;
//------------------------------------------
function myMap() {
	var myLoc = {lat: 39.85381551186161, lng: -104.67387513107451};
	var mapOptions = {
		center           : myLoc,
		zoom             : 18,
		noClear          : true,
		scrollwheel      : false,
		navigationControl: false,
		mapTypeControl   : false,
		scaleControl     : false,
		draggable        : true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
		/*
		mapTypeId: 'hybrid',
		heading: 90,
		tilt: 45
		*/
	}
	mainMap = new google.maps.Map(document.getElementById("map"), mapOptions);
	//--------------------------------------
	setBeacons(mainMap);
	setSniffer(mainMap);
	//--------------------------------------
return;
/***************************************************************************************************************************/
	var pinImage = new google.maps.MarkerImage("http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
		new google.maps.Size(40, 37),
		new google.maps.Point(0,0),
		new google.maps.Point(12, 35)
	);

	var marker = new google.maps.Marker({
		position: {lat:39.8530124646126, lng:-104.67393950409087},
		animation: google.maps.Animation.DROP,
		map: mainMap,
		draggable:true,
		title:'TEST PIN',
		icon: pinImage
	});
	var  tmpContent = ""+
			'<div>'+
				'<h4>Lat: '+0+'</h4>'+
				'<h4>Lng: '+0+'</h4>'+
			'</div>';
	(function(marker, i) {
		google.maps.event.addListener(marker, 'click', function() {
			infowindow = new google.maps.InfoWindow({
				content: tmpContent
			});
			infowindow.open(map, marker);
		});
	})(marker, 0);
	google.maps.event.addListener(marker, 'dragend', function(evt){
		tmpContent = ""+
			'<div>'+
				'<h4>Lat: '+evt.latLng.lat()+'</h4>'+
				'<h4>Lng: '+evt.latLng.lng()+'</h4>'+
			'</div>';
//		alert( evt.latLng.lat());
//		alert( evt.latLng.lng());
	});
/***************************************************************************************************************************/
}
//------------------------------------------

//------------------------------------------
function setBeacons(map){
	$.ajax({
		type: 'POST',
		url: '/ajax/myBeacons.php',
		dataType: 'json',
		error: function(xhr, textStatus, errorThrown){ console.log(xhr.status+" - "+errorThrown); },
		success: function(retVal){
			if(retVal.result==0){
				var pinImage = new google.maps.MarkerImage("http://maps.google.com/mapfiles/ms/icons/red-dot.png",
					new google.maps.Size(35, 32),
					new google.maps.Point(0,0),
					new google.maps.Point(10, 30)
				);
				for(var i in retVal.data){
					if(retVal.data[i].lat==null || retVal.data[i].lng==null){ continue; }
					if(retVal.data[i].lat==0    || retVal.data[i].lng==0   ){ continue; }
					var marker = new google.maps.Marker({
						position: {lat:Number(retVal.data[i].lat), lng:Number(retVal.data[i].lng)},
						animation: google.maps.Animation.DROP,
						map: map,
						draggable:false,
						title:retVal.data[i].beacon,
						icon: pinImage
					});

                    (function(marker, i) {
                        google.maps.event.addListener(marker, 'click', function() {
                            infowindow = new google.maps.InfoWindow({
                                content:
									'<div>'+
										/*
										'<h5>Gate Number: '+retVal.data[i].gate+'</h5>'+
										'<h6>Airline: '+retVal.data[i].airline+'</h6>'+
										'Machine: '+retVal.data[i].machine+
										'<h6>Scans: '+retVal.data[i].count+'</h6>'+
										*/
										'<h5>Name: '+retVal.data[i].name+'</h5>'+
										'<h5>Gate: '+retVal.data[i].gate+'</h5>'+
										'<h6>Airline: '+retVal.data[i].airline+'</h6>'+
//										'<h5>'+retVal.data[i].id+'</h5>'+
//										'UUID: '+retVal.data[i].uuid+''+
//										'<h6>major: '+retVal.data[i].major+'</h6>'+
//										'<h6>minor: '+retVal.data[i].minor+'</h6>'+
//										'<h6>Scans: '+retVal.data[i].scans+'</h6>'+
									'</div>'
                            });
                            infowindow.open(map, marker);
                        });
                    })(marker, i);
				}
			}else{ console.log(retVal.msg); }
		}
	});
}
//------------------------------------------

//------------------------------------------
function setSniffer(map){
	$.ajax({
		type: 'POST',
		url: '/ajax/mySniffer.php',
		dataType: 'json',
		error: function(xhr, textStatus, errorThrown){ console.log(xhr.status+" - "+errorThrown); },
		success: function(retVal){
			if(retVal.result==0){
				var pinImage = new google.maps.MarkerImage("http://maps.google.com/mapfiles/ms/icons/green-dot.png",
					new google.maps.Size(35, 32),
					new google.maps.Point(0,0),
					new google.maps.Point(10, 30)
				);
				for(var i in retVal.data){
					if(retVal.data[i].lat==null || retVal.data[i].lng==null){ continue; }
					if(retVal.data[i].lat==0    || retVal.data[i].lng==0   ){ continue; }
					var marker = new google.maps.Marker({
						position: {lat:Number(retVal.data[i].lat), lng:Number(retVal.data[i].lng)},
						animation: google.maps.Animation.DROP,
						map: map,
						draggable:false,
						title:retVal.data[i].beacon,
						icon: pinImage
					});

                    (function(marker, i) {
                        google.maps.event.addListener(marker, 'click', function() {
													var arrival = '';
													if(retVal.data[i].arrival){
														arrival = '<br><div>'+
															'<b>Arrival</b><br>'+
															'<div>Airline: '+retVal.data[i].arrival.airlineName+'</div>'+
															'<div>Arrival: '+retVal.data[i].arrival.currentTime+' '+retVal.data[i].arrival.currentDate+'</div>'+
															'<div>Flight No.: '+retVal.data[i].arrival.flightNumber+'</div>'+
															'<div>From: '+retVal.data[i].arrival.city+'</div>'+
														'</div>'
													}

													var departure = '';
													if(retVal.data[i].departure){
														departure = '<br><div>'+
															'<b>Departure</b><br>'+
															'<div>Airline: '+retVal.data[i].departure.airlineName+'</div>'+
															'<div>Departure: '+retVal.data[i].departure.currentTime+' '+retVal.data[i].departure.currentDate+'</div>'+
															'<div>Flight No.: '+retVal.data[i].departure.flightNumber+'</div>'+
															'<div>To: '+retVal.data[i].departure.city+'</div>'+
														'</div>'
													}



                            infowindow = new google.maps.InfoWindow({
                                content:
									'<div>'+
										'<h5>Gate: '+retVal.data[i].gate+'</h5>'+
										'<h6>Airline: '+retVal.data[i].airline+'</h6>'+
										'<h6>Date: '+retVal.data[i].date+'</h6>'+
										'<h6>Impressions: '+retVal.data[i].count+'</h6>'+
										'Average Dwell time: '+retVal.data[i].averageDwellTime+' min'+
									'</div>'+arrival+departure
                            });
                            infowindow.open(map, marker);
                        });
                    })(marker, i);
				}
			}else{ console.log(retVal.msg); }
		}
	});
}
//------------------------------------------

//------------------------------------------
function changeCenter(obj, lat, lng){
	mainMap.setCenter({lat:lat, lng:lng});
	$("button.btn.map-btn").each(function(){ $(this).attr('class', 'btn map-btn btn-info'); });
	$(obj).attr('class', 'btn map-btn btn-link');
	$("div#map").focus();
}
//------------------------------------------

//------------------------------------------
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?callback=myMap&key=AIzaSyBdqHS999I73IefOkY_xlGqG2LrdcDclxs"></script>
</html>
