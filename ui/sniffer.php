<html lang="en">
<head>
	<meta charset="<?=$ENCODING;?>" />
	<title>.</title>
	<meta http-equiv="refresh" content="300;" />
	<base href="<?= $SCHEME.'://'.$HOST.':'.$PORT.$BASE.'/'; ?>" />
	<link rel="stylesheet" href="ui/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="ui/css/bootstrap-table.css" type="text/css" />
	<link rel="stylesheet" href="ui/css/bootstrap-theme.min.css" type="text/css" />

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


	<script type="text/javascript" src="ui/js/jquery.min.js"></script>
	<script type="text/javascript" src="ui/js/jquery-ui.js"></script>
	<script type="text/javascript" src="ui/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="ui/js/bootstrap-table.js"></script>
		
	<style>
		table th{ text-align:center !important; }
	</style>
</head>
<body style="margin:10px;">
	<div style="width:50%;min-width:500px;height:95vh;margin:auto" id="base">
	<?php
	$mysql = mysqli_connect( 'localhost', 'root', 'EoMjulQ5yY', 'sniffer' );
	mysqli_query($mysql, "SET NAMES 'utf8'");
	if(mysqli_connect_errno()==0){
		$result = mysqli_query($mysql, "SELECT `eventday` FROM `daily_sum_aggregates` order by `eventday` desc limit 1");
		if( $result!=false ){
			$eventday = mysqli_fetch_assoc($result);
			$eventday = $eventday['eventday'];
			$result = mysqli_query($mysql, "SELECT * FROM `daily_sum_aggregates` where `eventday`='{$eventday}' and `beacon`<>'ap1_test_01'");
			if( $result!=false ){
			?>
				<table border="1">
				<thead>
					<th data-field="beacon" data-sortable=true  data-searchable=true  data-width='35%' data-align='center'>Beacon</th>
					<th data-field="count"  data-sortable=false data-searchable=false data-width='30%' data-align='right'>Total Number</th>
					<th data-field="date"   data-sortable=false data-searchable=false data-width='30%' data-align='center'>Date</th>
				</thead>
				<tbody>
				<?php while($row=mysqli_fetch_assoc($result)): ?>
				<tr>
					<td><?=$row['beacon'];?></td>
					<td style="text-align:right !important"><?=number_format($row['sum'], 0, '', ',');?></td>
					<td><?=date("m.d.Y", strtotime($row['eventday']));?></td>
				</tr>
				<?php endwhile; ?>
				</tbody>
				</table>
			<?php } ?>
		<?php } ?>
	<?php } ?>
	</div>
</body>
<script type="text/javascript">
//---------------------------------------------

//---------------------------------------------
$(function(){
	//-----------------------------------------
	$('table').bootstrapTable({
		locale   : 'en-US',
		height   : $("#base").height()-0,
		sortName : "beacon",
		sortOrder: "asc",
		uniqueId : 'id',
		search   : true,
		showFooter:false
	});
	//-----------------------------------------
	$.ajax({ 
		type: 'POST',
		url: '/set.php',
		dataType: 'json',
		error: function(xhr, textStatus, errorThrown){ console.log(xhr.status+" - "+errorThrown); },
		success: function(retVal){ console.log(retVal); }
	});
	//-----------------------------------------
})
//---------------------------------------------

//---------------------------------------------
</script>
</html>