<html lang="en">
<head>
	<meta charset="<?=$ENCODING;?>" />
	<title>.</title>
	<meta http-equiv="refresh" content="300;" />
	<base href="<?= $SCHEME.'://'.$HOST.':'.$PORT.$BASE.'/'; ?>" />
	<link rel="stylesheet" href="ui/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="ui/css/bootstrap-table.css" type="text/css" />
	<link rel="stylesheet" href="ui/css/bootstrap-theme.min.css" type="text/css" />

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


	<script type="text/javascript" src="ui/js/jquery.min.js"></script>
	<script type="text/javascript" src="ui/js/jquery-ui.js"></script>
	<script type="text/javascript" src="ui/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="ui/js/bootstrap-table.js"></script>
		
	<style>
		table th{ text-align:center !important; }
	</style>
</head>
<body style="margin:10px;">
	<div style="width:50%;min-width:500px;height:95vh;margin:auto" id="base">
	<?php
	$host='redshift-cluster1.cyekrcgfzfll.us-east-2.redshift.amazonaws.com';
	$db = 'dev';
	$username = 'redshift_master';
	$password = '2h<$fP3E[<rFZh{u';
	
	$dsn = "pgsql:host=$host;port=5439;dbname=$db;user=$username;password=$password";
	try{
		$conn = new PDO($dsn);
		if($conn){
			$query = 
				"select ".
					"eventday ".
				"from ".
					"daily_aggregates ".
				"order by ".
					"eventday desc ".
				"limit 1;";
			$statement = $conn->prepare($query);
			$statement->execute();
			$row = $statement->fetch();
			if($row==null){ $eventday = '-'; }
			else{ $eventday = $row['eventday']; }
			$query = 
				"select ".
					"SUM(count) as sum, eventday,beacon  ".
				"from ".
					"daily_aggregates ".
				"where ".
					"eventday = '{$eventday}' ".
				"group by ".
					"beacon,eventday ".
				"order by ".
					"eventday;".
				"";
			$statement = $conn->prepare($query);
			$statement->execute();
			$results = $statement->fetchAll();
			?>
			<table border="1">
			<thead>
				<th data-field="beacon" data-sortable=true  data-searchable=true  data-width='35%' data-align='center'>Beacon</th>
				<th data-field="count"  data-sortable=false data-searchable=false data-width='30%' data-align='right'>Total Number</th>
				<th data-field="date"   data-sortable=false data-searchable=false data-width='30%' data-align='center'>Date</th>
			</thead>
			<tbody>
			<?php foreach($results as $row): ?>
			<tr>
				<td><?=$row['beacon'];?></td>
				<td style="text-align:right !important"><?=number_format($row['sum'], 0, '', ',');?></td>
				<td><?=date("m.d.Y", strtotime($row['eventday']));?></td>
			</tr>
			<?php endforeach ?>
			</tbody>
			</table>
			<?php
		}else{ echo "Connection failed!"; }
	}catch (PDOException $e){ echo $e->getMessage(); }
	?>
	</div>
</body>
<script type="text/javascript">
var selected = [];
$(function(){
	$('table').bootstrapTable({
		locale   : 'en-US',
		height   : $("#base").height()-0,
		sortName : "beacon",
		sortOrder: "asc",
		uniqueId : 'id',
		search   : true,
		showFooter:false
	});
})
</script>
</html>